package com.of;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.of.pojo.NMember;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class XDemoTestApplicationTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setupMockMvc() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

//    @Test
//    public void insertDemoTest() throws Exception
//    {
//        NMember nMember = new NMember();
//        nMember.setId(5);
//        nMember.setAge(12);
//        nMember.setAddress("eee");
//        nMember.setBirthday(new Date());
//        nMember.setMonthlyincome(20.1);
//        nMember.setName("张三");
//
//        ObjectMapper mapper = new ObjectMapper();
//        //调用接口，传入添加的用户参数
//        mockMvc.perform(post("/xDemo/insertTest")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(mapper.writeValueAsString(nMember)))
////                .content(mapper.writeValueAsString(userInfo)))
//                //判断返回值，是否达到预期，测试示例中的返回值的结构如下{"errcode":0,"errmsg":"OK","p2pdata":null}
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                //使用jsonPath解析返回值，判断具体的内容
//                .andExpect(jsonPath("$.code", is("200")))
//                .andExpect(jsonPath("$.msg", notNullValue()))
//                .andExpect(jsonPath("$.data", not(0)))
//        ;
//    }
//
//    @Test
//    public void selectTest() throws Exception
//    {
//        ObjectMapper mapper = new ObjectMapper();
//        mockMvc.perform(post("/xDemo/selectTest")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
////                .andExpect(jsonPath("$.data", not(0)))
//                .andExpect(jsonPath("$.data.rowItemList[*].name",not(0)));
//    }
//
//    @Test
//    public void testJsonPath()
//    {
//        String str = "{\n" +
//                "\t\"isSuccess\": true,\n" +
//                "\t\"code\": \"200\",\n" +
//                "\t\"msg\": \"OK\",\n" +
//                "\t\"data\": {\n" +
//                "\t\t\"pageNum\": 1,\n" +
//                "\t\t\"pageSize\": 2,\n" +
//                "\t\t\"orderBy\": null,\n" +
//                "\t\t\"total\": 6,\n" +
//                "\t\t\"count\": false,\n" +
//                "\t\t\"totalPage\": 3,\n" +
//                "\t\t\"rowItemList\": [{\n" +
//                "\t\t\t\"id\": 1,\n" +
//                "\t\t\t\"name\": \"aaa\",\n" +
//                "\t\t\t\"age\": 20,\n" +
//                "\t\t\t\"birthday\": \"2018-03-06 00:00:00\",\n" +
//                "\t\t\t\"address\": \"aaa\",\n" +
//                "\t\t\t\"monthlyincome\": 12.0\n" +
//                "\t\t}, {\n" +
//                "\t\t\t\"id\": 2,\n" +
//                "\t\t\t\"name\": \"bbb\",\n" +
//                "\t\t\t\"age\": 35,\n" +
//                "\t\t\t\"birthday\": \"2018-03-06 00:00:00\",\n" +
//                "\t\t\t\"address\": \"bbb\",\n" +
//                "\t\t\t\"monthlyincome\": 13.0\n" +
//                "\t\t}],\n" +
//                "\t\t\"extObj\": null\n" +
//                "\t}\n" +
//                "}";
//
//        String strJsonPath = JsonPath.read(str,"$.data.rowItemList[0].name");
//        System.out.println("strJsonPath = " + strJsonPath);
//    }
}
