package com.of.XDemo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.util.StringUtil;
import com.of.XDemo.ao.XDemoAo;
import com.of.auth.service.AuthSystemService;
import com.of.constant.Const;
import com.of.controller.BaseController;
import com.of.enums.ErrorEnum;
import com.of.exception.BaseException;
import com.of.pojo.AuthRole;
import com.of.pojo.DataResult;
import com.of.pojo.LigerUIResult;
import com.of.pojo.NMember;
import com.of.pojo.NMemberQuery;
import com.of.pojo.Paging;
import com.of.utils.StringHelper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@RequestMapping("xDemo")
@Controller
@Slf4j
public class XDemoController extends BaseController {

    @Autowired
    private XDemoAo xDemoAo;

    @Reference(version = Const.COM_OF_VERSION, timeout = 25000)
    private AuthSystemService systemService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String demo() {
        return "xDemo";
    }

    /**
     * 用户注册
     *
     * @param nMember POJO
     * @return
     */
    @ApiOperation(value = "数据插入测试", notes = "插入一个值可以看看连接")
    @RequestMapping(value = "/insertTest", method = RequestMethod.POST
            , headers = {"content-type=application/json;charset=UTF-8", "content-type=application/xml;charset=UTF-8"}
            , consumes = {"application/json", "application/xml"}
    )
    public DataResult insertTest(@RequestBody @Valid NMember nMember) {
        return DataResult.ok(xDemoAo.insertDemoTest(nMember));
    }

    @ApiOperation(value = "数据删除测试", notes = "删除数据可以看看连接")
    @RequestMapping(value = "/deleteTest/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public DataResult deleteTest(@PathVariable("id") String id) {
        xDemoAo.deleteDemoTest(id);
        return DataResult.ok();
    }

    @ApiOperation(value = "数据修改测试", notes = "修改数据可以看看连接")
    @ApiImplicitParam(name = "tbTest", value = "tbTest", required = true, dataType = "TbTest")
    @RequestMapping(value = "/updateTestObj", method = RequestMethod.POST
    )
    public DataResult updateTest(@RequestBody NMember nMember) {
        return DataResult.ok(xDemoAo.updateDemoTest(nMember));
    }

    @ApiOperation(value = "数据分页查询测试", notes = "分页查询数据")
    @RequestMapping(value = "/selectTest", method = {RequestMethod.POST})
    @ResponseBody
    public LigerUIResult<NMember> selectTest(NMemberQuery nMemberQuery) {
        //查询获取数据
        Paging<NMember> pageInfo = xDemoAo.selectDemoTest(nMemberQuery);
        return getPage(pageInfo);
    }

    /**
     * 新增跳转页面
     *
     * @return
     */
//    @RequestMapping(value = "/addDemoTest", method = RequestMethod.GET)
//    public String addDemoTest() {
////        ModelAndView mav = new ModelAndView("/DemoAdd");
//        return "DemoAdd";
//    }
    @RequestMapping(value = "/addDemoTest", method = RequestMethod.GET)
    public ModelAndView addDemoTest() {
        ModelAndView mav = new ModelAndView("DemoAdd");
        return mav;
    }

    /**
     * 修改跳转页面
     *
     * @param id 的id
     * @return
     */
    @RequestMapping(value = "/editDemoTest/{id}", method = RequestMethod.GET)
    public ModelAndView editShow(@PathVariable("id") String id) {
        //参数检验
        if (StringUtil.isEmpty(id)) {
            throw new BaseException(ErrorEnum.NULL_OBJECT);
        }
        ModelAndView mav = new ModelAndView("/DemoUpd");
        //根据id获取对象
        NMember nMember = xDemoAo.getById(id);
        if (null != nMember) {
            mav.addObject("nMember", nMember);
        }

        return mav;
    }


    /**
     * 跳转INDEX主页面
     *
     * @return
     */
    @ApiOperation(value = "测试RequestPost", notes = "测试RequestPost")
    @RequestMapping(value = "/testIndex", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView testIndexPage() {
        ModelAndView mav = new ModelAndView("/Index");
        return mav;
    }

    /**
     * 跳转人员列表页面
     *
     * @return
     */
    @ApiOperation(value = "跳转人员列表页面", notes = "跳转人员列表页面")
    @RequestMapping(value = "/sUserList", method = RequestMethod.GET)
    public ModelAndView sUserList() {
        ModelAndView mav = new ModelAndView("/sUserList");
        return mav;
    }


    /**
     * 跳转人修改密码页面
     *
     * @return
     */
    @ApiOperation(value = "跳转人修改密码页面", notes = "跳转人修改密码页面")
    @RequestMapping(value = "/passwordPage/{id}", method = RequestMethod.GET)
    public ModelAndView changePasswordPage(@PathVariable("id") String id) {
        ModelAndView mav = new ModelAndView("/userPWD");
        mav.addObject("id", id);
        return mav;
    }

    /**
     * 跳转人员维护页面(新增)
     *
     * @return
     */
    @ApiOperation(value = "跳转人员维护页面", notes = "跳转人员维护页面")
    @RequestMapping(value = "/sUserAdd", method = RequestMethod.GET)
    public ModelAndView sUserAdd() {
        ModelAndView mav = new ModelAndView("/userAdd");
        return mav;
    }

    /**
     * 跳转人员维护页面(修改)
     *
     * @return
     */
    @ApiOperation(value = "跳转人员维护页面", notes = "跳转人员维护页面")
    @RequestMapping(value = "/sUserUpd/{id}", method = RequestMethod.GET)
    public ModelAndView sUserUpd(@PathVariable("id") String id) {
        ModelAndView mav = new ModelAndView("/userEdit");
        if (StringHelper.isNotBlank(id)) {
            mav.addObject("id", id);
        }
        return mav;
    }

    /**
     * 跳转角色列表页面
     *
     * @return
     */
    @ApiOperation(value = "跳转角色列表页面", notes = "跳转角色列表页面")
    @RequestMapping(value = "/sRoleList", method = RequestMethod.GET)
    public ModelAndView sRoleList() {
        ModelAndView mav = new ModelAndView("/sysRoleList");
        return mav;
    }

    /**
     * 跳转角色修改页面
     *
     * @return
     */
    @ApiOperation(value = "跳转角色修改页面", notes = "跳转角色修改页面")
    @RequestMapping(value = "/roleAddOrEditPage/{AddOrEdit}/{id}", method = RequestMethod.GET)
    public ModelAndView roleEditPage(@PathVariable("AddOrEdit") String AddOrEdit, @PathVariable("id") String id, AuthRole authRole) {
        ModelAndView mav = new ModelAndView("/sysRoleAdd");
        if (StringHelper.isNotBlank(id)) {
            //根据标识判断是新增还是修改如果是新增该ID为父类ID如果是修改则是本数据ID
            if ("Edit".equals(AddOrEdit)) {
                mav = new ModelAndView("/sysRoleEdit");
            }
            if ("XDEMO1234567890".equals(id)) {
                mav.addObject("id", "");
            } else {
                mav.addObject("id", id);
            }

            mav.addObject("authRole", authRole);
        }
        return mav;
    }

    /**
     * 跳转菜单列表页面
     *
     * @return
     */
    @ApiOperation(value = "跳转菜单列表页面", notes = "跳转菜单列表页面")
    @RequestMapping(value = "/sMenuList", method = RequestMethod.GET)
    public ModelAndView sMenuList() {
        ModelAndView mav = new ModelAndView("/sysMenuList");
        return mav;
    }
}
