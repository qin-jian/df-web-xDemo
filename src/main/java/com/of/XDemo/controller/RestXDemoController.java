//package com.of.XDemo.controller;
//
//import com.of.controller.BaseController;
//import com.of.rest.RestService;
//import com.of.utils.JsonHelper;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RequestMapping(value = "/xrest")
//@RestController
//public class RestXDemoController extends BaseController {
//
//    @Autowired
//    private RestService restService;
//
//    @ApiOperation(value = "测试HTTP", notes = "测试HTTP")
//    @RequestMapping(value = "/testHttp", method = RequestMethod.GET)
//    public String testHttp() {
//        String result = restService.get("http://127.0.0.1:8798/xDemo");
//        return result;
//    }
//
//    @ApiOperation(value = "测试RequestPost", notes = "测试RequestPost")
//    @RequestMapping(value = "/testRequestPost", method = RequestMethod.GET)
//    @ResponseBody
//    public ModelAndView testPage()
//    {
////        Map<String, String> headers = new HashMap<>();
////        headers.put("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
////        String result = restService.get("http://127.0.0.1:8698/api/xDemo/testRest");
//        ModelAndView mav = new ModelAndView("/xDemo");
//        return mav;
//    }
//
//}
