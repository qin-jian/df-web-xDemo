package com.of.XDemo.ao;

import com.alibaba.dubbo.config.annotation.Reference;
import com.of.XDemo.service.XDemoService;
import com.of.auth.service.AuthSystemService;
import com.of.constant.Const;
import com.of.pojo.NMember;
import com.of.pojo.NMemberQuery;
import com.of.pojo.Paging;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class XDemoAo {

    @Reference(version = Const.COM_OF_VERSION, timeout = 25000)
    private XDemoService demoTestService;

    @Reference(version = Const.COM_OF_VERSION, timeout = 25000)
    private AuthSystemService authSystemService;

    /**
     * 测试新增
     *
     * @param nMember
     * @return
     */
    public int insertDemoTest(NMember nMember) {
        return demoTestService.insertDemoTest(nMember);

    }

    /**
     * 测试删除
     *
     * @param id
     */
    public void deleteDemoTest(String id) {
        demoTestService.deleteDemoTest(id);
    }

    /**
     * 测试修改
     *
     * @param nMember
     * @return
     */
    public NMember updateDemoTest(NMember nMember) {
        return demoTestService.updateDemoTest(nMember);
    }

    /**
     * 测试分页
     *
     * @param nMemberQuery
     * @return
     */
    public Paging selectDemoTest(NMemberQuery nMemberQuery) {
        return demoTestService.selectDemoTest(nMemberQuery);
    }

    /**
     * 根据ID获取对象数据
     *
     * @param id
     * @return
     */
    public NMember getById(String id) {
        return demoTestService.getById(id);
    }

    public ArrayList<NMember> findList() {
        return demoTestService.findList();
    }


}
