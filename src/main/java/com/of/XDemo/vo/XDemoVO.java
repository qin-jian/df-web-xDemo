package com.of.XDemo.vo;

import com.of.pojo.AuthMenu;

import java.util.List;

public class XDemoVO {
    List<AuthMenu> data;

    public List<AuthMenu> getData() {
        return data;
    }

    public void setData(List<AuthMenu> data) {
        this.data = data;
    }
}
