/**
 * Created by cyqresig on 15-8-19.
 */


//[ligerGrid自适应高度]
var ligGridDefaultHeight;
//[ligerGrid自适应页数据显示条数]
var ligGridDefaultPageSize;

//[ligerGrid自适应高度]
//根据页面情况，动态计算grid应占用的高度
function getLigerGridFixedHeight() {
    var gridRowHeight = 29;
    var gridBottomBarHeight = 32;
    var gridHeaderHeight = 28 + 1;
    var gridHorizontalScrollBarHeight = 17;

    var gridCount = $('.prolist-form').length;
    //var ligGirdVerticalMarign = (gridCount == 1 ? 30 : 20*gridCount);
//    var containerWidth = 10; //排除外容器的内边距5*2
    var ligGridOtherGap = 2;    //每个ligGrid有未计算到位的3px
    //排除计算grid元素.prolist-form，以及纯外边距元素-form.mt10的占用高度计算
    var girdDimensionHeight = parseInt($('#container').css('height'), 10);// - containerWidth;// - ligGirdVerticalMarign;
//    console.log('girdDimensionHeight ->' + girdDimensionHeight)
    var lastMarginBottom = 0;
    var marginTop = 0;
    var $o, outerHeight;
    var displayFlag;
    var $children = $('#container').children();
    var childrenLength = $children.length;
    $.each($children, function(i, o) {
        displayFlag = false;
        $o = $(o);
        //当只有上下外边距，并且容器内没有其他元素时，不计算这个容器的高度
        if($o[0].className == 'mt10' && $o.children().length == 0) {
            return true;
        }
        marginTop = parseInt($o.css('margin-top'), 10);

//        console.log($o);
//        console.log('marginTop -> ' + marginTop);
//        console.log('$o.outerHeight() ->' + $o.outerHeight());
//        console.log('lastMarginBottom -> ' + lastMarginBottom);

        outerHeight = $o.outerHeight();

        //当上下容器存在外边距合并时，只取最大的外边距进行计算
        girdDimensionHeight = girdDimensionHeight - outerHeight - Math.max(lastMarginBottom, marginTop);

        lastMarginBottom =  parseInt($o.css('margin-bottom'), 10);
//        console.log('lastMarginBottom -> ' + lastMarginBottom);

        if(i == childrenLength - 1) {
            girdDimensionHeight = girdDimensionHeight - lastMarginBottom;
        }

    });
//    console.log('girdDimensionHeight -> ' + girdDimensionHeight)
    if(Object.prototype.toString.call(gridCount).toLowerCase() == '[object number]' && gridCount > 1) {
        girdDimensionHeight /= gridCount;
    }

    girdDimensionHeight = girdDimensionHeight - ligGridOtherGap;
    if(girdDimensionHeight < (gridRowHeight * 5 + gridBottomBarHeight + gridHeaderHeight + gridHorizontalScrollBarHeight - ligGridOtherGap)) {
        girdDimensionHeight = gridRowHeight * 5 + gridBottomBarHeight + gridHeaderHeight + gridHorizontalScrollBarHeight - ligGridOtherGap;
    }
//    console.log('girdDimensionHeight -> ' + girdDimensionHeight)
//    console.log('$(window).innerHeight() - > ' + $(window).innerHeight())
//    console.log('$(container).css(height) - > ' + $('#container').css('height'));
    return girdDimensionHeight;
}

//[ligerGrid自适应页数据显示条数]
//根据动态计算grid应占用的高度的情况，计算出应显示多少条数据，不够5保留5，超过5进位
function getLigGridDefaultPageSize(girdDimensionHeight) {
    var ligGridOtherGap = 2;    //每个ligGrid有未计算到位的3px
    var gridRowHeight = 29;
    var gridBottomBarHeight = 32;
    var gridHeaderHeight = 28 + 1;
    var gridHorizontalScrollBarHeight = 17;
    var gridContentHeight = ligGridDefaultHeight - gridBottomBarHeight - gridHeaderHeight - gridHorizontalScrollBarHeight;
    var gridDefaultPageSize = Math.floor(gridContentHeight / gridRowHeight);    //向下舍入移除小数点
    //console.log('gridDefaultPageSize -> ' + gridDefaultPageSize);
    if(gridDefaultPageSize < 5) {
        gridDefaultPageSize = 5;
    }
    else {
        //5->5, 6->10, 11->15, 16->20...
        gridDefaultPageSize = ( Math.floor(gridDefaultPageSize / 5) + 1 ) * 5;
    }
    //console.log('gridDefaultPageSize -> ' + gridDefaultPageSize);

    if(girdDimensionHeight == (gridRowHeight * 5 + gridBottomBarHeight + gridHeaderHeight + gridHorizontalScrollBarHeight - ligGridOtherGap)) {
        gridDefaultPageSize = 5;
    }

    return gridDefaultPageSize;
}

//[ligerGrid实时编辑框，随着ligerGrid内容区域水平滚动条移动（已修改ligerUI源码，在ligerGrid内容区域加入了这个滚动事件）]
//obj表示ligerGrid内容区域的dom源生对象
function scrollGridEditor(obj) {
//    console.log(obj)
    var $obj = $(obj);
    var $ligerGrid = $obj.parent().parent().parent().parent().parent();
//    console.log($ligerGrid);
    var $l_grid_editor = $ligerGrid.find('.l-grid-editor');
//    console.log($l_grid_editor);
    var lastScrollLeft, scrollLeft, offsetX;
    if($l_grid_editor.length > 0) {
        lastScrollLeft = $.data(obj, 'scrollLeft') || 0;
//        console.log('lastScrollLeft -> ' + lastScrollLeft);
        scrollLeft = $obj.scrollLeft();
        offsetX = scrollLeft - lastScrollLeft;
//        console.log('offsetX -> ' + offsetX);
        $l_grid_editor.css('left', parseInt($l_grid_editor.css('left'), 10) - offsetX);
//        console.log('$obj.scrollLeft() -> ' + $obj.scrollLeft());
    }
    $.data(obj, 'scrollLeft', $obj.scrollLeft());
}

//ligerGrid每页显示条数切换后触发事件[已对应修改ligerUI对应的源码部分]
function f_AfterPageSizeChange(pageSize, g) {
    var ligGridOtherGap = 2;    //每个ligGrid有未计算到位的3px
    var gridRowHeight = 29;
    var gridBottomBarHeight = 32;
    var gridHeaderHeight = 28 + 1;
    var gridHorizontalScrollBarHeight = 17;
    //根据每页能显示的最大条数，重设置grid的高度
    g.options.height = gridRowHeight * (pageSize || 5) + gridBottomBarHeight + gridHeaderHeight + gridHorizontalScrollBarHeight - ligGridOtherGap;
}

//合并ligerGrid配置的方法
function setfixedLigerGridSettings(codeSettings, userSettings) {
    //codeSettings指直接写在页面上的ligerGrid全部配置
    //userSettings指从后台读出的ligerGrid部分配置
    var fixedUserSettingsObj;

    $.each(codeSettings.columns, function(i, codeSettingsObj) {

        $.each(userSettings.columns, function(j, userSettingsObj) {
            fixedUserSettingsObj = {
                width: userSettingsObj.gc_width,
                hide: !userSettingsObj.gc_visible,
                sort: userSettingsObj.gc_sort,
                columnindex: userSettingsObj.gc_sort,
                name: userSettingsObj.grid_column_name,
                display: userSettingsObj.grid_column_details
            };
            if(codeSettingsObj.name == fixedUserSettingsObj.name) {
                $.extend(codeSettingsObj, fixedUserSettingsObj);
            }
        });

    });

//    codeSettings.columns.sort(function(item1, item2) {
////        console.log('item1.display - > ' + item1.display);
////        console.log('item1.sort - > ' + item1.sort);
////        console.log('item2.display - > ' + item2.display);
////        console.log('item2.sort - > ' + item2.sort);
////        console.log('item1.sort > item2.sort - >' + (item1.sort > item2.sort));
//        return item1.sort > item2.sort;
//    });

    bubbleSort(codeSettings.columns, function(item1, item2) {
        return item1.sort < item2.sort;
    });
}

function bubbleSort(array, compare){
    var i = 0, len = array.length,
        j, d;
    for(; i<len; i++){
        for(j=0; j<len; j++){
            //if(array[i] < array[j]){
            if( compare(array[i], array[j]) ) {
                d = array[j];
                array[j] = array[i];
                array[i] = d;
            }
        }
    }
}

function openLigerUserOption(grid_name,appServer) {
    $.dialog({
        id:'ligerUserOption',
        title:"列表配置",
        width:"700"+'px',
        height:"500"+'px',
        lock: true,
        resize: false ,
        max: false ,
        min:false,
        fixed: true,
        content:"url:"+appServer+"/gridColumnUserSetting/gridColumnUserSettingEditPage?grid_name=" + grid_name
    });
}

    var $document = $(document);

//$(function() {
    $('body').css('overflow-x', 'hidden');      //禁止body水平滚动条
    $('table.formWrap02').wrap('<div></div>');  //为防止firefox下jquery直接获取表格高度的bug
    $('#container').css('height', $(window).innerHeight() - 10);    //带ligerGrid页面的主容器高度，设置为内容区域高度-10
    //[ligerGrid自适应高度]
    ligGridDefaultHeight = getLigerGridFixedHeight();
    //console.log('ligGridDefaultHeight -> ' + ligGridDefaultHeight);
    //[ligerGrid自适应页数据显示条数]
    ligGridDefaultPageSize = getLigGridDefaultPageSize(ligGridDefaultHeight);
//    console.log('ligGridDefaultPageSize -> ' + ligGridDefaultPageSize);
//});

//绑定带ligerGrid查询文本框的回车事件

    $('.formWrap02 :input:text')
        .on('focusin', function() {
            $document.on('keydown.search', function(e) {
                if(e.keyCode == 13) {
                    $('#btnSearch').click();
                }
            });
        })
        .on('focusout', function() {
            $document.off('keydown.search');
        });