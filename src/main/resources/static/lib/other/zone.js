//[""|hour|0]
function getZoneVal(bl){
	if(bl){
		var re=false;
		$('#tbodyContent tr [name="hour"]').each(function(){
			var hour=$(this).val();
			if(isNaN(hour) || Number(hour)<=0){
				re=true;
			}
		});
		if(re){
			alert("请输入大于零的服务小时数！");
			return;
		}
	}
	var arr=new Array();
	$("#tbodyContent tr").each(function(){
		// province city  area street hour enable
		var itemStr="";
		var $current=$(this);
		if($current.find('[name="street"]').val()){
			itemStr +=$current.find('[name="street"]').val()+"|";
		}
		else if($current.find('[name="area"]').val()){
			itemStr +=$current.find('[name="area"]').val()+"|";
		}
		else if($current.find('[name="city"]').val()){
			itemStr +=$current.find('[name="city"]').val()+"|";
		}
		else if($current.find('[name="province"]').val()){
			itemStr +=$current.find('[name="province"]').val()+"|";
		}
		else{
			itemStr +="|";
		}
		itemStr +=($current.find('[name="hour"]').val()||"")+"|";
		if($current.find('[name="enable"]').prop("checked")){
			itemStr +="0";
		}
		else{
			itemStr +="1";
		}
		arr.push(itemStr);
	});
	return arr;
}

function editZone(zoneVal){
	var zoneArr=zoneVal.split(",");
	var timeout=500;
	for(var k=0;k<zoneArr.length;k++){
		var tempArr=zoneArr[k].split("|");
		$("#tbodyContent").append($("#tbodyDemo").html());
		var provinceStr="<option value=\"\">请选择</option>";
		for(var m=0;m<zone.length;m++) {
			provinceStr +="<option value="+zone[m].a+">"+zone[m].b+"</option>";
		}
		$('[name="province"]:last').html(provinceStr);
		if(tempArr[0].length==2){
			$('[name="province"]:last').val(tempArr[0]);
			for(var i=0;i<zone.length;i++) {
				if(zone[i].a==tempArr[0]){
					var cityStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[i].c.length;j++) {
						cityStr +="<option value="+zone[i].c[j].a+">"+zone[i].c[j].b+"</option>";
					}
					$('[name="province"]:last').parent().parent().find('[name="city"]').attr("indexnum",i);
					$('[name="province"]:last').parent().parent().find('[name="city"]').html(cityStr);
				}
			}
		}
		else if(tempArr[0].length==4){
			$('[name="province"]:last').val(tempArr[0].substr(0,2));
			for(var i=0;i<zone.length;i++) {
				if(zone[i].a==tempArr[0].substr(0,2)){
					var cityStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[i].c.length;j++) {
						cityStr +="<option value="+zone[i].c[j].a+">"+zone[i].c[j].b+"</option>";
					}
					$('[name="province"]:last').parent().parent().find('[name="city"]').attr("indexnum",i);
					$('[name="province"]:last').parent().parent().find('[name="city"]').html(cityStr);
				}
			}
			$('[name="city"]:last').val(tempArr[0]);
			var indexnum=$('[name="city"]:last').attr("indexnum");
			for(var i=0;i<zone[indexnum].c.length;i++) {
				if(zone[indexnum].c[i].a==tempArr[0]){
					var areaStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[indexnum].c[i].c.length;j++) {
						areaStr +="<option value="+zone[indexnum].c[i].c[j].a+">"+zone[indexnum].c[i].c[j].b+"</option>";
					}
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum",indexnum);
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum2",i);
					$('[name="city"]:last').parent().parent().find('[name="area"]').html(areaStr);
				}
			}
		}
		else if(tempArr[0].length==6){
			$('[name="province"]:last').val(tempArr[0].substr(0,2));
			for(var i=0;i<zone.length;i++) {
				if(zone[i].a==tempArr[0].substr(0,2)){
					var cityStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[i].c.length;j++) {
						cityStr +="<option value="+zone[i].c[j].a+">"+zone[i].c[j].b+"</option>";
					}
					$('[name="province"]:last').parent().parent().find('[name="city"]').attr("indexnum",i);
					$('[name="province"]:last').parent().parent().find('[name="city"]').html(cityStr);
				}
			}
			$('[name="city"]:last').val(tempArr[0].substr(0,4));
			var indexnum=$('[name="city"]:last').attr("indexnum");
			for(var i=0;i<zone[indexnum].c.length;i++) {
				if(zone[indexnum].c[i].a==tempArr[0].substr(0,4)){
					var areaStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[indexnum].c[i].c.length;j++) {
						areaStr +="<option value="+zone[indexnum].c[i].c[j].a+">"+zone[indexnum].c[i].c[j].b+"</option>";
					}
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum",indexnum);
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum2",i);
					$('[name="city"]:last').parent().parent().find('[name="area"]').html(areaStr);
				}
			}

			$('[name="area"]:last').val(tempArr[0]);
			var indexnum=$('[name="area"]:last').attr("indexnum");
			var indexnum2=$('[name="area"]:last').attr("indexnum2");

			for(var i=0;i<zone[indexnum].c[indexnum2].c.length;i++) {
				if(zone[indexnum].c[indexnum2].c[i].a==tempArr[0]){
					var streetStr="<option value=\"\">请选择</option>";
					if(zone[indexnum].c[indexnum2].c[i].c){//非市辖区
						for(var j=0;j<zone[indexnum].c[indexnum2].c[i].c.length;j++) {
							streetStr +="<option value="+zone[indexnum].c[indexnum2].c[i].c[j].a+">"+zone[indexnum].c[indexnum2].c[i].c[j].b+"</option>";
						}
					}
					$('[name="area"]:last').parent().parent().find('[name="street"]').html(streetStr);
				}
			}

		}
		else if(tempArr[0].length==9){
			$('[name="province"]:last').val(tempArr[0].substr(0,2));
			for(var i=0;i<zone.length;i++) {
				if(zone[i].a==tempArr[0].substr(0,2)){
					var cityStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[i].c.length;j++) {
						cityStr +="<option value="+zone[i].c[j].a+">"+zone[i].c[j].b+"</option>";
					}
					$('[name="province"]:last').parent().parent().find('[name="city"]').attr("indexnum",i);
					$('[name="province"]:last').parent().parent().find('[name="city"]').html(cityStr);
				}
			}
			$('[name="city"]:last').val(tempArr[0].substr(0,4));
			var indexnum=$('[name="city"]:last').attr("indexnum");
			for(var i=0;i<zone[indexnum].c.length;i++) {
				if(zone[indexnum].c[i].a==tempArr[0].substr(0,4)){
					var areaStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[indexnum].c[i].c.length;j++) {
						areaStr +="<option value="+zone[indexnum].c[i].c[j].a+">"+zone[indexnum].c[i].c[j].b+"</option>";
					}
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum",indexnum);
					$('[name="city"]:last').parent().parent().find('[name="area"]').attr("indexnum2",i);
					$('[name="city"]:last').parent().parent().find('[name="area"]').html(areaStr);
				}
			}

			$('[name="area"]:last').val(tempArr[0].substr(0,6));
			var indexnum=$('[name="area"]:last').attr("indexnum");
			var indexnum2=$('[name="area"]:last').attr("indexnum2");

			for(var i=0;i<zone[indexnum].c[indexnum2].c.length;i++) {
				if(zone[indexnum].c[indexnum2].c[i].a==tempArr[0].substr(0,6)){
					var streetStr="<option value=\"\">请选择</option>";
					if(zone[indexnum].c[indexnum2].c[i].c){//非市辖区
						for(var j=0;j<zone[indexnum].c[indexnum2].c[i].c.length;j++) {
							streetStr +="<option value="+zone[indexnum].c[indexnum2].c[i].c[j].a+">"+zone[indexnum].c[indexnum2].c[i].c[j].b+"</option>";
						}
					}
					$('[name="area"]:last').parent().parent().find('[name="street"]').html(streetStr);
				}
			}
			$('[name="street"]:last').val(tempArr[0]);
		}

		$('[name="hour"]:last').val(tempArr[1]);
		if(tempArr[2]==0){
			$('[name="enable"]:last').prop("checked",true);
		}
		if(tempArr[2]==1){
			$('[name="enable"]:last').prop("checked",false);
		}
	}
}

function creatZone(){
	$("#tbodyContent").append($("#tbodyDemo").html());
	var provinceStr="<option value=\"\">请选择</option>";
	for(var i=0;i<zone.length;i++) {
		provinceStr +="<option value="+zone[i].a+">"+zone[i].b+"</option>";
	}
	$('[name="province"]:last').html(provinceStr);
}

(function($) {
	$(document).on("click",'[ebtn="add"]',function(e){
		creatZone();
	});

	$(document).on("click",'[ebtn="deleteZone"]',function(e){
		var $target=$(e.target);
		$target.parent().parent().remove();
	});

	$(document).on("change",'[name="province"]',function(e){
		var $target=$(e.target);
		if($target.val()){
			for(var i=0;i<zone.length;i++) {
				if(zone[i].a==$target.val()){
					var cityStr="<option value=\"\">请选择</option>";
					for(var j=0;j<zone[i].c.length;j++) {
						cityStr +="<option value="+zone[i].c[j].a+">"+zone[i].c[j].b+"</option>";
					}
					$target.parent().parent().find('[name="city"]').attr("indexnum",i);
					$target.parent().parent().find('[name="city"]').html(cityStr);
					$target.parent().parent().find('[name="area"]').html("<option value=\"\">请选择</option>");
					$target.parent().parent().find('[name="street"]').html("<option value=\"\">请选择</option>");
				}
			}
		}
		else{
			$target.parent().parent().find('[name="city"]').html("<option value=\"\">请选择</option>");
			$target.parent().parent().find('[name="area"]').html("<option value=\"\">请选择</option>");
			$target.parent().parent().find('[name="street"]').html("<option value=\"\">请选择</option>");
		}

	});

	$(document).on("change",'[name="city"]',function(e){
		var $target=$(e.target);
		var indexnum=$target.attr("indexnum");
		if($target.val()) {
			for (var i = 0; i < zone[indexnum].c.length; i++) {
				if (zone[indexnum].c[i].a == $target.val()) {
					var areaStr = "<option value=\"\">请选择</option>";
					for (var j = 0; j < zone[indexnum].c[i].c.length; j++) {
						areaStr += "<option value=" + zone[indexnum].c[i].c[j].a + ">" + zone[indexnum].c[i].c[j].b + "</option>";
					}
					$target.parent().parent().find('[name="area"]').attr("indexnum", indexnum);
					$target.parent().parent().find('[name="area"]').attr("indexnum2", i);
					$target.parent().parent().find('[name="area"]').html(areaStr);
					$target.parent().parent().find('[name="street"]').html("<option value=\"\">请选择</option>");
				}
			}
		}
		else{
			$target.parent().parent().find('[name="area"]').html("<option value=\"\">请选择</option>");
			$target.parent().parent().find('[name="street"]').html("<option value=\"\">请选择</option>");
		}
	});

	$(document).on("change",'[name="area"]',function(e){
		var $target=$(e.target);
		var indexnum=$target.attr("indexnum");
		var indexnum2=$target.attr("indexnum2");

		for(var i=0;i<zone[indexnum].c[indexnum2].c.length;i++) {
			if(zone[indexnum].c[indexnum2].c[i].a==$target.val()){
				var streetStr="<option value=\"\">请选择</option>";
				if(zone[indexnum].c[indexnum2].c[i].c){//非市辖区
					for(var j=0;j<zone[indexnum].c[indexnum2].c[i].c.length;j++) {
						streetStr +="<option value="+zone[indexnum].c[indexnum2].c[i].c[j].a+">"+zone[indexnum].c[indexnum2].c[i].c[j].b+"</option>";
					}
				}
				$target.parent().parent().find('[name="street"]').html(streetStr);
			}
		}

	});
})(jQuery);