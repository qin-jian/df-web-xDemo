<%@ page import="java.util.Enumeration" %>
<%@ page import="com.doorto.site.common.context.PlatformContext" %>
<%@ page import="com.doorto.site.common.enums.BOExceptionEnum" %>
<%@ page import="com.doorto.exception.BOException" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@page language="java" pageEncoding="UTF-8" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content=zh-cn http-equiv=Content-Language>
	<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
    <link rel="stylesheet" href="../static/css/error.css"/>
</head>
<body>
<div class="info">
    <img src="../static/images/info.jpg"/>
    出错啦
    <%
        /*Enumeration<String> enums = request.getParameterNames();
        while(enums.hasMoreElements()) {
            String s = enums.nextElement();
            out.println(s);
        }*/
        if(exception instanceof BOException){
//            if(BOExceptionEnum.OUT_TIME_LOGIN.errorCode().equals(((BOException) exception).getErrorCode())){
//                PlatformContext.gologin();
//            }
        }else{
            Logger logger = LoggerFactory.getLogger(getClass());
            logger.error(exception.getLocalizedMessage());
//            out.println(exception.getLocalizedMessage());
        }
//        out.println(((BOException)exception).getErrorCode());
    %>
</div>

</body>
</html>