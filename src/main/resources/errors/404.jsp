<%@page language="java" pageEncoding="UTF-8" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content=zh-cn http-equiv=Content-Language>
	<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
    <link rel="stylesheet" href="../static/css/error.css"/>
</head>
<body>
<div class="info">
    <img src="../static/images/info.jpg"/>
    您所访问的地址不存在
</div>

</body>
</html>